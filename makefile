ASM = nasm
ASMFLAGS = -f elf64 -g
LD = ld

%.o: %.inc
	$(ASM) $(ASMFLAGS) -o $@ $<

lib.o: lib.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

dict.o: dict.asm lib.o
	$(ASM) $(ASMFLAGS) -o $@ $<

main.o: main.asm words.o lib.o dict.o
	$(ASM) $(ASMFLAGS) -o $@ $<

program: main.o words.o dict.o lib.o
	$(LD) -o $@ $^

clean:
	$(RM) *.o
	$(RM) program

all: program

.PHONY: clean all
