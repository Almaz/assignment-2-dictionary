%include "words.inc"
%include "lib.inc"
%include "dict.inc"
%define STDIN 0
%define STDOUT 1
%define STDERR 2

global _start

section .data

error: db "No elements with this key were found", 0
key_buff: times 255 db 0

section .text

_start:
    mov rdx, 255
    mov rsi, key_buff
    call read_text ; читаем что ввел пользователь
    mov r8, rax ; сохраянем длину ключа в r8
    mov rdi, test1
    mov rsi, key_buff
    push r8
    call find_word ; находим элемент с соответствующим ключем
    pop r8
    cmp rax, 0 ; если элемент не найден - идем в bad
    je .bad
    mov rdi, rax ; кладем указатель на элемент в rdi
    add rdi, 8 ; добавляем к rdi 8 байт чтобы получить адрес ключа
    add rdi, r8 ; добавляем к rdi длину ключа, чтобы получить адрес значения
    inc rdi ; стоит учесть терминирование строки, прибавляем единицу
    mov r9, STDOUT ; выводим в stdout
    call print_string ; выводим значение
    call print_newline
    xor rdi, rdi ; кладем в rdi код ошибки 0
    call exit
    .bad:
    mov rdi, error ; кладем указатель на сообщение об ошибки в rdi
    mov r9, STDERR ; выводим в stderr
    call print_string ; выводим ошибку
    call print_newline
    mov rdi, 1 ; кладем в rdi код ошибки 1
    call exit

read_text:
    mov rdi, STDIN ; читаем из stdin
    mov rax, 0
    syscall
    dec rax ; уменьшаем размер строки на 1, т.к. мы не хотим учитывать символ новой строки
    mov byte [rsi+rax], 0 ; заменяем символ новой строки на 0 (терминируем строку)
    ret
