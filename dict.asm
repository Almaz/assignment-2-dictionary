%include "lib.inc"

global find_word

section .text

; находит элемент словаря по ключу и возвращает его в rax
find_word:
    .loop:
    cmp rdi, 0 ; если указатель на словарь 0 - идем в bad
    je .bad
    push rdi ; сохраняем calee-saved регистры
    add rdi, 8 ; rdi указывает на начало элемента списка (т.е. на адрес след элемента)
    ; добавим 8 байт, чтобы он указывал на ключ, а не на адрес след элемента
    call string_equals ; проверям равны ли ключи
    pop rdi ; возвращаем callee-saved регистры
    cmp rax, 0 ; если ключи совпали - идем в good
    jne .good
    mov rdi, [rdi] ; переходим к следующему элементу
    jmp .loop
    .good:
    mov rax, rdi ; кладем указатель на найденный элемент в rax
    ret
    .bad:
    xor rax, rax ; кладем 0 в rax если элемент не найден
    ret
